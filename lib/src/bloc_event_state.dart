import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc.dart';

abstract class BlocEvent extends Object {}
abstract class BlocState extends Object {}

abstract class BlocEventState<BlocEvent, BlocState> extends Bloc {
  PublishSubject<BlocEvent> _eventController = PublishSubject<BlocEvent>();
  BehaviorSubject<BlocState> _stateController = BehaviorSubject<BlocState>();

  /// To be invoked to emit an event
  Function(BlocEvent) get emitEvent => _eventController.sink.add;

  /// Current/New state
  Stream<BlocState> get state => _stateController.stream;

  /// Last State
  BlocState get lastState => _stateController.value;

  /// External processing of the event
  Stream<BlocState> eventHandler(BlocEvent event, BlocState currentState);

  /// initialState
  final BlocState initialState;

  BlocEventState({
    @required this.initialState,
  }){
    // invoke the [eventHandler] for each received event and emit any resulting newState
    _eventController.listen((BlocEvent event){
      BlocState currentState = lastState ?? initialState;
      eventHandler(event, currentState).forEach((BlocState newState){
        _stateController.sink.add(newState);
      });
    });
  }

  @override
  void dispose() {
    _eventController.close();
    _stateController.close();
  }
}